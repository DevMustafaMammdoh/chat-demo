require('./bootstrap');

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

import Vue from 'vue';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

 const files = require.context('./', true, /\.vue$/i)
 files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
 
 /**
  * Next, we will create a fresh Vue application instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */
 
 const app = new Vue({
     el: '#app',
 
     data: {
         messages: [],
         users: [],
     },
 
     created() {
         this.fetchMessages();
         if(! window.room_id){
         Echo.join('chat')
             .here(users => {
                console.log(users);
                 this.users = users;
             })
             .joining(user => {
                 console.log(user);
                 this.users.push(user);
             })
             .leaving(user => {
                 this.users = this.users.filter(u => u.id !== user.id);
             })
             .listenForWhisper('typing', ({id, name}) => {
                 console.log('typing');
                 this.users.forEach((user, index) => {
                     if (user.id === id) {
                         user.typing = true;
                         this.$set(this.users, index, user);
                     }
                 });
             })
             .listen('MessageSent', (event) => {
                 this.messages.push({
                     message: event.message.message,
                     user: event.user
                 });
 
                 this.users.forEach((user, index) => {
                     if (user.id === event.user.id) {
                         user.typing = false;
                         this.$set(this.users, index, user);
                     }
                 });
             });
        }


          if(window.room_id){
            this.users.push(window.user);
            Echo.private(`room.${window.room_id}`)
            .listen('MessageSentInRoom', (event) => {
                console.log(event);
                this.messages.push({
                    message: event.message.message,
                    user: event.message.user
                });
            });
          }   
     },
 
     methods: {
         fetchMessages() {
             if(! window.room_id){
             axios.get('/messages').then(response => {
                 this.messages = response.data;
             });
             }

             if(window.room_id){
                axios.get('/roomMessages/' + window.room_id).then(response => {
                    this.messages = response.data;
                });
             }
         },
 
         addMessage(message) {
             this.messages.push(message);
 
             axios.post('/messages', message).then(response => {
                 console.log(response.data);
             });
         },

         addMessageToRoom(message) {
             console.log(message);
            this.messages.push(message);
            axios.post('/roomMessages', message).then(response => {
                console.log(response.data);
            });
        }
     }
 });
