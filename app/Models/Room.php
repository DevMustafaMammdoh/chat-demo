<?php

namespace App\Models;

use App\Models\User;
use App\Models\Message;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Room extends Model
{
    use HasFactory;
    protected $fillable = ['creator_id','receiver_id'];

    public function creator()
    {
    	return $this->belongsTo(User::class,'creator_id');
    }

    public function receiver()
    {
    	return $this->belongsTo(User::class,'receiver_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
