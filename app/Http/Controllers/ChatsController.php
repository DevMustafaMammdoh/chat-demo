<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\User;
use App\Models\Message;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use App\Events\MessageSentInRoom;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('chat');
    }

    public function fetchMessages()
    {
        return Message::with('user')->whereNull('room_id')->get();
    }

    public function sendMessage(Request $request)
    {
        $message = auth()->user()->messages()->create([
            'message' => $request->message
        ]);

		broadcast(new MessageSent(auth()->user(), $message))->toOthers();

        return ['status' => 'Message Sent!'];
    }

    public function indexForPrivate(User $user){

        $room = Room::where(function($q)use($user){
           $q->where('receiver_id',$user->id)->Where('creator_id',auth()->id());
        })->orWhere(function($q)use($user){
            $q->where('receiver_id',auth()->id())->Where('creator_id',$user->id);
         })->first();

         if(! $room)
         {
           $room = Room::create(['receiver_id'=>$user->id,'creator_id'=>auth()->id()]);
         }
         $room->load(['creator','receiver']);
         return view('chatPrivate',['room'=>$room]);
    }

    public function fetchMessageForRoom(Room $room)
    {
        return Message::with('user')->where('room_id',$room->id)->get();
    }

    public function sendMessageInRoom(Request $request)
    {
        $room = Room::findOrFail($request->room_id);

        $message = $room->messages()->create([
            'message' => $request->message,
            'user_id'=>auth()->id()
        ]);

        $message->load('user');

		broadcast(new MessageSentInRoom($room, $message))->toOthers();

        return ['status' => 'Message Sent!'];
    }

}
