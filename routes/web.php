<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChatsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/chat', [ChatsController::class,'index']);
Route::get('messages', [ChatsController::class,'fetchMessages']);
Route::post('messages', [ChatsController::class,'sendMessage']);
Route::get('privatChat/{user}', [ChatsController::class,'indexForPrivate']);
Route::get('roomMessages/{room}', [ChatsController::class,'fetchMessageForRoom']);
Route::post('roomMessages', [ChatsController::class,'sendMessageInRoom']);


require __DIR__.'/auth.php';
